#!/usr/bin/env bash

curl -X POST http://localhost:9200/jobs/_bulk \
	-H "Expect:" \
	-H "Content-Type: application/json; charset=utf-8" \
	--data-binary @- << EOF
	{"index":{}}
	{"name": "Dobroslav Čopák", "position": "lakovník/lakovač", "city": "Prešov"}
	{"index":{}}
	{"name": "Jarolím Kuderjavý", "position": "technik bozp", "city": "Spišská Nová Ves"}
	{"index":{}}
	{"name": "Vendelín Hisem", "position": "podlahár, dláždič", "city": "Ružomberok"}
	{"index":{}}
	{"name": "Ján Vraňuch", "position": "fakturant", "city": "Bytča"}
	{"index":{}}
	{"name": "Oto Želonka", "position": "obuvník", "city": "Bratislava"}
	{"index":{}}
	{"name": "Agáta Humeňaj", "position": "grafický dizajnér", "city": "Trnava"}
	{"index":{}}
	{"name": "Alojz Ištván", "position": "bytový architekt", "city": "Prievidza"}
	{"index":{}}
	{"name": "Andrej Grohoľ", "position": "maliar, natierač", "city": "Košice"}
	{"index":{}}
	{"name": "Dezider Dubovský", "position": "servisný technik", "city": "Brezno"}
	{"index":{}}
	{"name": "Štefan Ščecina", "position": "lektor, školiteľ", "city": "Rimavská Sobota"}
	{"index":{}}
	{"name": "Sergej Havrilla", "position": "pekár", "city": "Trnava"}
	{"index":{}}
	{"name": "Radomír Gabányi", "position": "finančný poradca", "city": "Trnava"}
	{"index":{}}
	{"name": "Ladislav Hajduk", "position": "programátor", "city": "Michalovce"}
	{"index":{}}
	{"name": "Ján Dzurík", "position": "redaktor", "city": "Prievidza"}
	{"index":{}}
	{"name": "Alfonz Mihalov", "position": "správca počítačovej siete", "city": "Poprad"}
	{"index":{}}
	{"name": "Dušan Demjanovič", "position": "tlmočník", "city": "Ružomberok"}
	{"index":{}}
	{"name": "Servác Duplinský", "position": "projektant", "city": "Piešťany"}
	{"index":{}}
	{"name": "Filip Gerka", "position": "tanečník", "city": "Bardejov"}
	{"index":{}}
	{"name": "Albín Norko", "position": "sklár", "city": "Prievidza"}
	{"index":{}}
	{"name": "Michal Perečinský", "position": "statik", "city": "Brezno"}
	{"index":{}}
	{"name": "Agáta Petrík", "position": "pekár", "city": "Prievidza"}
	{"index":{}}
	{"name": "Sergej Križan", "position": "stylista, vizážista", "city": "Trebišov"}
	{"index":{}}
	{"name": "Gašpar Gerčák", "position": "maskér, parochniar", "city": "Michalovce"}
	{"index":{}}
	{"name": "Albert Kolarčík", "position": "editor", "city": "Levice"}
	{"index":{}}
	{"name": "Silvester Vlasák", "position": "fakturant", "city": "Martin"}
	{"index":{}}
	{"name": "Imrich Vraník", "position": "fyzioterapeut", "city": "Rimavská Sobota"}
	{"index":{}}
	{"name": "Pankrác Kuruc", "position": "pracovný psychológ", "city": "Topoľčany"}
	{"index":{}}
	{"name": "František Turčok", "position": "opravár", "city": "Bardejov"}
	{"index":{}}
	{"name": "Dalibor Wittner", "position": "elektrikár", "city": "Zvolen"}
	{"index":{}}
	{"name": "Jarolím Gogoľ", "position": "pracovný psychológ", "city": "Trebišov"}
	{"index":{}}
	{"name": "Igor Országh", "position": "automechanik", "city": "Rimavská Sobota"}
	{"index":{}}
	{"name": "Daniel Tokarčík", "position": "fyzioterapeut", "city": "Bytča"}
	{"index":{}}
	{"name": "Pravoslav Pitlík", "position": "prekladateľ", "city": "Považská Bystrica"}
	{"index":{}}
	{"name": "Svätopluk Tkačík", "position": "web dizajnér", "city": "Považská Bystrica"}
	{"index":{}}
	{"name": "Samuel Beňa", "position": "redaktor", "city": "Nové Zámky"}
	{"index":{}}
	{"name": "Ctibor Frajt", "position": "tanečník", "city": "Banská Bystrica"}
	{"index":{}}
	{"name": "Svetozár Bidovský", "position": "technik bozp", "city": "Liptovský Mikuláš"}
	{"index":{}}
	{"name": "Karol Lörinc", "position": "taxikár", "city": "Ružomberok"}
	{"index":{}}
	{"name": "Gašpar Šoltis", "position": "tlmočník", "city": "Zvolen"}
	{"index":{}}
	{"name": "Oliver Ľaš", "position": "strihač", "city": "Trnava"}
	{"index":{}}
	{"name": "Marek Bednár", "position": "elektrotechnik", "city": "Rimavská Sobota"}
	{"index":{}}
	{"name": "Maximilián Šišovský", "position": "odevný návrhár, konštruktér strihov", "city": "Trnava"}
	{"index":{}}
	{"name": "Alfréd Antuš", "position": "kozmetička", "city": "Považská Bystrica"}
	{"index":{}}
	{"name": "Kamil Czaja", "position": "finančný poradca", "city": "Lučenec"}
	{"index":{}}
	{"name": "Radoslav Gavrún", "position": "grafický dizajnér", "city": "Martin"}
	{"index":{}}
	{"name": "Svätopluk Stanek", "position": "grafik", "city": "Trnava"}
	{"index":{}}
	{"name": "Medard Kopčák", "position": "kozmetička", "city": "Považská Bystrica"}
	{"index":{}}
	{"name": "Samuel Langer", "position": "stolárstvo", "city": "Prievidza"}
	{"index":{}}
	{"name": "Blahoslav Girmala", "position": "autoelektrikár", "city": "Piešťany"}
	{"index":{}}
	{"name": "Zora Kendereš", "position": "vodič kamiónu", "city": "Ružomberok"}
	{"index":{}}
	{"name": "František Feťko", "position": "lektor, školiteľ", "city": "Lučenec"}
	{"index":{}}
	{"name": "Patrik Bednár", "position": "kameraman", "city": "Komárno"}
	{"index":{}}
	{"name": "Roman Sovák", "position": "grafik", "city": "Spišská Nová Ves"}
	{"index":{}}
	{"name": "Izidor Humeník", "position": "lektor, školiteľ", "city": "Bytča"}
	{"index":{}}
	{"name": "Zlatko Fedorčák", "position": "správca budov", "city": "Poprad"}
	{"index":{}}
	{"name": "Gregor Peťo", "position": "kameraman", "city": "Košice"}
	{"index":{}}
	{"name": "Jaroslav Matiaš", "position": "editor", "city": "Lučenec"}
	{"index":{}}
	{"name": "Zdenko Balik", "position": "garderobier", "city": "Topoľčany"}
	{"index":{}}
	{"name": "Miloslav Vernarský", "position": "novinár", "city": "Martin"}
	{"index":{}}
	{"name": "Konštantín Šarocký", "position": "programátor", "city": "Bratislava"}
	{"index":{}}
	{"name": "Fedor Polča", "position": "rozpočtár/kalkulant", "city": "Topoľčany"}
	{"index":{}}
	{"name": "Pavol Balint", "position": "záhradník", "city": "Trebišov"}
	{"index":{}}
	{"name": "Vendelín Hocko", "position": "opravár", "city": "Ružomberok"}
	{"index":{}}
	{"name": "Ľuboš Kundľa", "position": "elektrikár", "city": "Martin"}
	{"index":{}}
	{"name": "Jarolím Štefaník", "position": "Športový tréner", "city": "Liptovský Mikuláš"}
	{"index":{}}
	{"name": "Slavomír Kuropčák", "position": "elektromechanik", "city": "Poprad"}
	{"index":{}}
	{"name": "Radoslav Cimbala", "position": "kozmetička", "city": "Brezno"}
	{"index":{}}
	{"name": "Cyprián Kendický", "position": "systémový administrátor", "city": "Žilina"}
	{"index":{}}
	{"name": "Eugen Antal", "position": "obuvník", "city": "Poprad"}
	{"index":{}}
	{"name": "Slavomír Petraško", "position": "projektant", "city": "Trenčín"}
	{"index":{}}
	{"name": "Drahoslav Šandala", "position": "lektor, školiteľ", "city": "Žilina"}
	{"index":{}}
	{"name": "Maximilián Sabol", "position": "strihač", "city": "Rimavská Sobota"}
	{"index":{}}
	{"name": "Jakub Beňa", "position": "projektant", "city": "Lučenec"}
	{"index":{}}
	{"name": "Benjamín Birčák", "position": "bytový architekt", "city": "Ružomberok"}
	{"index":{}}
	{"name": "Imrich Oľšavský", "position": "pekár", "city": "Banská Bystrica"}
	{"index":{}}
	{"name": "Miloslav Petriľák", "position": "copywriter", "city": "Komárno"}
	{"index":{}}
	{"name": "Ján Frimmer", "position": "maskér, parochniar", "city": "Bytča"}
	{"index":{}}
	{"name": "Emanuel Dobrovič", "position": "elektrikár", "city": "Piešťany"}
	{"index":{}}
	{"name": "Agáta Balog", "position": "kameraman", "city": "Rimavská Sobota"}
	{"index":{}}
	{"name": "Miloš Birkner", "position": "copywriter", "city": "Rimavská Sobota"}
	{"index":{}}
	{"name": "Zora Vojtko", "position": "inštalatér", "city": "Ružomberok"}
	{"index":{}}
	{"name": "Drahoslav Kertés", "position": "revízny technik", "city": "Bratislava"}
	{"index":{}}
	{"name": "Ivan Dzuričko", "position": "coach", "city": "Prievidza"}
	{"index":{}}
	{"name": "Agáta Priputen", "position": "broker", "city": "Poprad"}
	{"index":{}}
	{"name": "Dobroslav Smolka", "position": "web dizajnér", "city": "Bratislava"}
	{"index":{}}
	{"name": "Krištof Burda", "position": "stylista, vizážista", "city": "Považská Bystrica"}
	{"index":{}}
	{"name": "Blažena Pavuk", "position": "elektromechanik", "city": "Zvolen"}
	{"index":{}}
	{"name": "Jaroslav Kiseľa", "position": "redaktor", "city": "Bardejov"}
	{"index":{}}
	{"name": "Anton Alexovič", "position": "inštalatér", "city": "Spišská Nová Ves"}
	{"index":{}}
	{"name": "Ľuboš Mihály", "position": "korektor", "city": "Žilina"}
	{"index":{}}
	{"name": "Benjamín Bory", "position": "editor", "city": "Košice"}
	{"index":{}}
	{"name": "Radoslav Korba", "position": "programátor", "city": "Humenné"}
	{"index":{}}
	{"name": "Augustín Krátky", "position": "servisný technik", "city": "Košice"}
	{"index":{}}
	{"name": "Agáta Ščecina", "position": "webmaster", "city": "Piešťany"}
	{"index":{}}
	{"name": "Drahomír Klučár", "position": "programátor", "city": "Rimavská Sobota"}
	{"index":{}}
	{"name": "Mário Vypušťák", "position": "grafický dizajnér", "city": "Trenčín"}
	{"index":{}}
	{"name": "Móric Zachar", "position": "architekt", "city": "Bardejov"}
	{"index":{}}
	{"name": "Henrich Makula", "position": "copywriter", "city": "Levice"}
	{"index":{}}
	{"name": "Stanislav Pavlovič", "position": "dtp operátor", "city": "Liptovský Mikuláš"}
	{"index":{}}
	{"name": "Drahoslav Roguľa", "position": "dizajnér", "city": "Prievidza"}
	{"index":{}}
	{"name": "Miroslav Uhrín", "position": "kozmetička", "city": "Levice"}
	{"index":{}}
	{"name": "Adam Slebodník", "position": "podlahár, dláždič", "city": "Humenné"}
	{"index":{}}
	{"name": "Sergej Biľ", "position": "fotograf", "city": "Košice"}
	{"index":{}}
	{"name": "Václav Mrva", "position": "kaderník", "city": "Michalovce"}
	{"index":{}}
	{"name": "Félix Slaminka", "position": "grafický dizajnér", "city": "Nitra"}
	{"index":{}}
	{"name": "Vladimír Horvát", "position": "korektor", "city": "Žilina"}
	{"index":{}}
	{"name": "Edmund Kočiš", "position": "rozpočtár/kalkulant", "city": "Trnava"}
	{"index":{}}
	{"name": "Jaroslav Mašlej", "position": "klampiar, pokrývač", "city": "Nové Zámky"}
	{"index":{}}
	{"name": "Jaromír Bulík", "position": "nábytkár", "city": "Bratislava"}
	{"index":{}}
	{"name": "Bohumil Ivanov", "position": "seo analytik", "city": "Levice"}
	{"index":{}}
	{"name": "Konštantín Uebersax", "position": "sprievodca v cestovnom ruchu,", "city": "Bratislava"}
	{"index":{}}
	{"name": "Beňadik Kozár", "position": "konzultant", "city": "Levice"}
	{"index":{}}
	{"name": "Štefan Smrek", "position": "automechanik", "city": "Levice"}
	{"index":{}}
	{"name": "Dalibor Dubovický", "position": "webmaster", "city": "Zvolen"}
	{"index":{}}
	{"name": "Viktor Wintner", "position": "pekár", "city": "Rimavská Sobota"}
	{"index":{}}
	{"name": "Peter Gabčo", "position": "záhradný architekt", "city": "Považská Bystrica"}
	{"index":{}}
	{"name": "Arnold Fecko", "position": "freelancer", "city": "Trenčín"}
	{"index":{}}
	{"name": "Ignác Kiseľa", "position": "Športový tréner", "city": "Prešov"}
	{"index":{}}
	{"name": "Zora Gruška", "position": "novinár", "city": "Trebišov"}
	{"index":{}}
	{"name": "Milan Kocan", "position": "grafik", "city": "Žilina"}
	{"index":{}}
	{"name": "Vojtech Goliaš", "position": "finančný poradca", "city": "Spišská Nová Ves"}
	{"index":{}}
	{"name": "Boris Glankovič", "position": "tesár", "city": "Trenčín"}
	{"index":{}}
	{"name": "Dávid Talpaš", "position": "tlmočník", "city": "Rimavská Sobota"}
	{"index":{}}
	{"name": "Ferdinand Grohoľ", "position": "podlahár, dláždič", "city": "Trebišov"}
	{"index":{}}
	{"name": "Marek Jedinák", "position": "opravár", "city": "Piešťany"}
	{"index":{}}
	{"name": "Alan Lörinc", "position": "bytový architekt", "city": "Trnava"}
	{"index":{}}
	{"name": "Zoltán Filakovský", "position": "sprievodca v cestovnom ruchu,", "city": "Piešťany"}
	{"index":{}}
	{"name": "Maximilián Bandy", "position": "technik bozp", "city": "Nitra"}
	{"index":{}}
	{"name": "Karol Laubert", "position": "vodič autobusu", "city": "Levice"}
	{"index":{}}
	{"name": "Matej Čech", "position": "elektrikár", "city": "Liptovský Mikuláš"}
	{"index":{}}
	{"name": "Teodor Poláček", "position": "strihač", "city": "Levice"}
	{"index":{}}
	{"name": "Henrich Balún", "position": "obuvník", "city": "Žilina"}
	{"index":{}}
	{"name": "Jaromír Pecuch", "position": "web dizajnér", "city": "Nitra"}
	{"index":{}}
	{"name": "Silvester Scholtés", "position": "záhradný architekt", "city": "Levice"}
	{"index":{}}
	{"name": "Svetozár Klimovič", "position": "projektant", "city": "Považská Bystrica"}
	{"index":{}}
	{"name": "Ernest Michalov", "position": "rozpočtár/kalkulant", "city": "Považská Bystrica"}
	{"index":{}}
	{"name": "Richard Bramuška", "position": "mäsiar", "city": "Piešťany"}
	{"index":{}}
	{"name": "Dezider Jenčo", "position": "freelancer", "city": "Piešťany"}
	{"index":{}}
	{"name": "Adam Šuťak", "position": "dtp operátor", "city": "Lučenec"}
	{"index":{}}
	{"name": "Július Džobanik", "position": "fakturant", "city": "Trenčín"}
	{"index":{}}
	{"name": "Levoslav Velgos", "position": "sklár", "city": "Nitra"}
	{"index":{}}
	{"name": "Servác Karlik", "position": "prekladateľ", "city": "Bytča"}
	{"index":{}}
	{"name": "Dalibor Andrejčák", "position": "masér", "city": "Považská Bystrica"}
	{"index":{}}
	{"name": "Tibor Gdovin", "position": "technik bozp", "city": "Košice"}
	{"index":{}}
	{"name": "Kamil Talpaš", "position": "systémový administrátor", "city": "Martin"}
	{"index":{}}
	{"name": "Pravoslav Drbjak", "position": "tesár", "city": "Rimavská Sobota"}
	{"index":{}}
	{"name": "Koloman Olejčuk", "position": "opravár", "city": "Nitra"}
	{"index":{}}
	{"name": "Vladislav Salamon", "position": "fotograf", "city": "Trenčín"}
	{"index":{}}
	{"name": "Marián Zakuťanský", "position": "lakovník/lakovač", "city": "Michalovce"}
	{"index":{}}
	{"name": "Kamil Varchola", "position": "maliar, natierač", "city": "Prešov"}
	{"index":{}}
	{"name": "Eugen Klimko", "position": "sklár", "city": "Humenné"}
	{"index":{}}
	{"name": "Jakub Velgos", "position": "grafický dizajnér", "city": "Nitra"}
	{"index":{}}
	{"name": "Juraj Szentesi", "position": "masér", "city": "Poprad"}
	{"index":{}}
	{"name": "Pravoslav Konival", "position": "statik", "city": "Topoľčany"}
	{"index":{}}
	{"name": "Oto Končár", "position": "realitný maklér", "city": "Humenné"}
	{"index":{}}
	{"name": "Koloman Kovalčík", "position": "tanečník", "city": "Topoľčany"}
	{"index":{}}
	{"name": "Arnold Andrejčák", "position": "stavebný dozor", "city": "Bardejov"}
	{"index":{}}
	{"name": "Cyprián Berecký", "position": "automechanik", "city": "Nové Zámky"}
	{"index":{}}
	{"name": "Roland Tomčák", "position": "stolár", "city": "Banská Bystrica"}
	{"index":{}}
	{"name": "Fedor Goban", "position": "stylista, vizážista", "city": "Martin"}
	{"index":{}}
	{"name": "Rudolf Balara", "position": "choreograf", "city": "Košice"}
	{"index":{}}
	{"name": "Slavomír Leško", "position": "stylista, vizážista", "city": "Humenné"}
	{"index":{}}
	{"name": "Blažena Ružbacký", "position": "záhradník", "city": "Košice"}
	{"index":{}}
	{"name": "Félix Redaj", "position": "technik bozp", "city": "Spišská Nová Ves"}
	{"index":{}}
	{"name": "Rastislav Stolárik", "position": "stavbyvedúci", "city": "Michalovce"}
	{"index":{}}
	{"name": "Ľudomil Uhriňák", "position": "kozmetička", "city": "Banská Bystrica"}
	{"index":{}}
	{"name": "Jarolím Lopuchovský", "position": "daňový poradca", "city": "Martin"}
	{"index":{}}
	{"name": "Leonard Peregrin", "position": "nábytkár", "city": "Lučenec"}
	{"index":{}}
	{"name": "Ladislav Cziel", "position": "elektromechanik", "city": "Prievidza"}
	{"index":{}}
	{"name": "Augustín Torma", "position": "sklár", "city": "Lučenec"}
	{"index":{}}
	{"name": "Kazimír Krafčík", "position": "rozpočtár/kalkulant", "city": "Brezno"}
	{"index":{}}
	{"name": "Drahomír Klobušovský", "position": "grafický dizajnér", "city": "Topoľčany"}
	{"index":{}}
	{"name": "Miroslav Holomáň", "position": "prekladateľ", "city": "Bardejov"}
	{"index":{}}
	{"name": "Imrich Stašík", "position": "elektromechanik", "city": "Topoľčany"}
	{"index":{}}
	{"name": "Peter Rusnák", "position": "redaktor", "city": "Banská Bystrica"}
	{"index":{}}
	{"name": "Peter Černý", "position": "podlahár, dláždič", "city": "Trebišov"}
	{"index":{}}
	{"name": "Pavol Gruľ", "position": "systémový administrátor", "city": "Spišská Nová Ves"}
	{"index":{}}
	{"name": "Cyril Kundračík", "position": "servisný technik", "city": "Košice"}
	{"index":{}}
	{"name": "Fridrich Lengyel", "position": "stavebný dozor", "city": "Poprad"}
	{"index":{}}
	{"name": "Ladislav Bechera", "position": "architekt", "city": "Trenčín"}
	{"index":{}}
	{"name": "Félix Marinica", "position": "freelancer", "city": "Komárno"}
	{"index":{}}
	{"name": "Gašpar Baloga", "position": "zvukár", "city": "Zvolen"}
	{"index":{}}
	{"name": "Eduard Andrejovský", "position": "masér", "city": "Nitra"}
	{"index":{}}
	{"name": "Ignác Tomčík", "position": "grafik", "city": "Bytča"}
	{"index":{}}
	{"name": "Filip Karaffa", "position": "podlahár, dláždič", "city": "Banská Bystrica"}
	{"index":{}}
	{"name": "Marek Marinčák", "position": "maliar, natierač", "city": "Nitra"}
	{"index":{}}
	{"name": "Agáta Kandra", "position": "garderobier", "city": "Prešov"}
	{"index":{}}
	{"name": "Daniel Konival", "position": "inštalatér", "city": "Brezno"}
	{"index":{}}
	{"name": "Viliam Dučay", "position": "sprievodca v cestovnom ruchu,", "city": "Komárno"}
	{"index":{}}
	{"name": "Igor Bury", "position": "finančný poradca", "city": "Trnava"}
	{"index":{}}
	{"name": "Richard Iľko", "position": "grafik", "city": "Zvolen"}
	{"index":{}}
	{"name": "Jozef Juričko", "position": "bytový architekt", "city": "Poprad"}
	{"index":{}}
	{"name": "Stanislav Želonka", "position": "systémový administrátor", "city": "Martin"}
	{"index":{}}
	{"name": "Silvester Černý", "position": "kaderník", "city": "Zvolen"}
	{"index":{}}
	{"name": "Izidor Geci", "position": "opravár", "city": "Nitra"}
	{"index":{}}
	{"name": "Kristián Bačinský", "position": "záhradný architekt", "city": "Košice"}
	{"index":{}}
	{"name": "Fedor Bado", "position": "choreograf", "city": "Prievidza"}
	{"index":{}}
	{"name": "Bohumír Karaffa", "position": "freelancer", "city": "Banská Bystrica"}
	{"index":{}}
	{"name": "Stanislav Fejko", "position": "kaderník", "city": "Trnava"}
	{"index":{}}
	{"name": "Samuel Macejka", "position": "konzultant", "city": "Nitra"}
EOF
