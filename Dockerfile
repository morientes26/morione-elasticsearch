FROM docker.elastic.co/elasticsearch/elasticsearch:7.9.2
MAINTAINER Michal Kalman <kalman@morione.sk>

ADD config/ /usr/share/elasticsearch/config
