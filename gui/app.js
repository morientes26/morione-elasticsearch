const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
const client = require("./connection.js");

const port = process.env.PORT || 5000;

app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true })); 

app.get('/',function(req, res){
  res.sendFile(path.join(__dirname+'/index.html'));
});
app.post('/',function(req, res){
  client.search({
    index: 'jobs',
     body: { "size": 300}   
    }).then(function(resp) {
    	console.log("Query: ", req.body.query);
        console.log("Successful query! Count of results: ", resp.hits.total.value);
        res.send(resp);
    }, function(err) {
        console.trace(err.message);
        res.send(err.message);
    });
});

app.post('/search', function(req, res){
    let queries = req.body.query.split(" ");
    let query = req.body.query;
    let queryPrefix = " and ";
    let qCount = 1;
    if (query && query.includes(" ")){
        let n = query.lastIndexOf(" ");
        queryPrefix += query.substr(n+1);
        query = "";
        for (i = 0; i < queries.length - 1; i++) {
            query+=queries[i] + "~ "
        }
        qCount = queries.length;
    } else {
        queryPrefix = "";
    }

console.log(query)
console.log(queryPrefix)
    console.log(qCount)

    client.search({
        index: 'jobs',
        body: {
          "query": {
            "query_string": {
              "fields": [
                "name",
                "position",
                "city"
              ],
              "query": query + queryPrefix + "*",
              "type": "cross_fields",
              "minimum_should_match": qCount
            }
          }
        }   
    }).then(function(resp) {
    	console.log("Query: ", req.body.query);
        console.log("Successful query! Count of results: ", resp.hits.total.value);
        res.send(resp);
    }, function(err) {
        console.trace(err.message);
        res.send(err.message);
    });
});


app.listen(port, function(){
	console.log('Running at Port', port);
});
