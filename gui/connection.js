var elasticsearch = require("elasticsearch");

const ES_URL = process.env.ES || "http://localhost:9200";
console.log("ElasticSearch url: ", ES_URL);

var client = new elasticsearch.Client({
  hosts: [ES_URL]
});

module.exports = client;