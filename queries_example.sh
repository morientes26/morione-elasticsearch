curl -i http://localhost:9200/jobs/_search?pretty -d ' {
    "size": 10,
    "query": {
		"match_all" : {
		}
	}
}'

curl -i http://localhost:9200/jobs/_search?pretty -d ' {
    "size": 10, 
    "query": {
        "match" : {
            "name" : "Ján"
        }
	}
}'

curl -i http://localhost:9200/jobs/_search?pretty -d ' {
    "size": 10, 
    "query": {
        "match_phrase_prefix" : {
            "name" : {
            	"query":"Já"
            }
        }
	}
}'

# multi checkovanie viacerych poloziek
# best_fields vyhlada najlepsie skore
# explain attribut vysvetli ziskanu query
curl -i http://localhost:9200/jobs/_search?pretty -d ' {
    "size": 10,
    "explain": true, 
    "query": {
    	"multi_match" : {
      		"query":    "Menzák", 
      		"type":     "best_fields",
      		"fields": [ "name", "text" ] 
    	}
  	}
}'

# toto mi nefunguje ale mal by to byt priamy match
# _source - obmedzi vystup len na zadane atributy
curl -i http://localhost:9200/jobs/_search?pretty -d ' {
    "size": 10, 
    "_source": ["name"],
    "query": {
    	"term" : {
      		"city": "Levice"
    	}
  	}
}'

# okrem priameho matchu aj agregovanie vysledku 
curl -i http://localhost:9200/jobs/_search?pretty -d ' {
    "query": {
    	"match" : {
      		"city" : "Zvolen"
    	}
  	},
  	"aggs" : {
        "agregacia_miest" : {
            "terms" : { "field" : "city" }
		} 
	}
}'

# hladanie nad viacerymi atributmi iba s prefixom
curl -i http://localhost:9200/jobs/_search?pretty -d ' { 
  "query": {
    "query_string": {
      "fields": [
        "name",
        "profesion",
        "city"
      ],
      "query": "stol*"
    }
  }
}'

#  hladanie nad viacerymi atributmi iba s prefixom
curl -i http://localhost:9200/jobs/_search?pretty -d ' { 
  "_source": ["text"],
  "query": {
    "multi_match": {
      "fields": [
        "name",
        "profesion",
        "city"
      ],
      "query": "stolárčina",
      "type": "phrase_prefix"
    }
  }
}'