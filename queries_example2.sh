{
    query: {
      bool: {
        should: [
          {
            bool: {
              must: [
                { match: { "club.country_id": country.id } },
                { match: { name: query } }
              ]
            }
          },
          {
            bool: {
              must: [
                { match: { country_id: country.id } },
                { match: { name: query } }
              ]
            }
          }
        ],
        minimum_should_match: 1
      }
    }
  }

# sucasny multi match na vsetky polozky.  Funguje ako autosuggest ale
# len na zaciatocne frazy. Nefunguje fuzzy, cize preklepy

curl -X GET http://localhost:9200/jobs/_search?pretty \
  -H "Expect:" \
  -H "Content-Type: application/json; charset=utf-8" \
  --data-binary @- << EOF
  {
    "query": {
      "multi_match": {
        "fields": [
          "name",
          "position",
          "city"
        ],
        "query": "Bla",
        "type": "phrase_prefix"
      }
    }
  }
EOF

# toto funguje na preklep, ale nie ako autocomplite, kde potrebujem phrase_prefix

curl -X GET http://localhost:9200/jobs/_search?pretty \
  -H "Expect:" \
  -H "Content-Type: application/json; charset=utf-8" \
  --data-binary @- << EOF
  {
    "query": {
      "match": {     
        "city" : {   
          "query": "brtislava",
          "fuzziness": "auto"
        }
      }
    }
  }
EOF

curl -X GET http://localhost:9200/jobs/_search?pretty \
  -H "Expect:" \
  -H "Content-Type: application/json; charset=utf-8" \
  --data-binary @- << EOF
  {
    "query": {
      "match": {     
        "city.keywordstring" : {   
          "query": "Bratislav",
          "analyzer": "slovencina"
        }
      }
    }
  }
EOF

# toto je standard na autocompletion. Treba este skusit aj synonyma respektive slovencina analyzer
# a tiez multi match
curl -X GET http://localhost:9200/jobs/_search?pretty \
  -H "Expect:" \
  -H "Content-Type: application/json; charset=utf-8" \
  --data-binary @- << EOF
{
  "suggest": {
    "city-suggest-fuzzy": {
      "prefix": "peter brtislava",
      "completion": {
        "field": "city.completion",
        "fuzzy": {
          "fuzziness": "auto"
        }
      }
    }
  }
}
EOF

# funguje ale iba ak query je na konkretny field
curl -X GET http://localhost:9200/jobs/_search?pretty \
  -H "Expect:" \
  -H "Content-Type: application/json; charset=utf-8" \
  --data-binary @- << EOF
{
  "suggest": {
    "name-suggest-fuzzy": {
      "prefix": "pete",
      "completion": {
        "field": "name.completion",
        "fuzzy": {
          "fuzziness": "auto"
        }
      }
    },
    "position-suggest-fuzzy": {
      "prefix": "stola",
      "completion": {
        "field": "position.completion",
        "fuzzy": {
          "fuzziness": "auto"
        }
      }
    },
    "city-suggest-fuzzy": {
      "prefix": "brtislava",
      "completion": {
        "field": "city.completion",
        "fuzzy": {
          "fuzziness": "auto"
        }
      }
    }
  }
}
EOF

# nefunguje
curl -X GET http://localhost:9200/jobs/_search?pretty \
  -H "Expect:" \
  -H "Content-Type: application/json; charset=utf-8" \
  --data-binary @- << EOF
{
  "suggest": {
    "summary-suggest-fuzzy": {
      "prefix": "Peter Šento stolárstvo Bratislava",
      "completion": {
        "field": "summary.completion",
        "fuzzy": {
          "fuzziness": "auto"
        }
      }
    }
  }
}
EOF

# ak pridam jeden computed field a aplikujem jednoduchy match tak celkom fajn vysledky
curl -X GET http://localhost:9200/jobs/_search\?pretty \
  -H "Expect:" \
  -H "Content-Type: application/json; charset=utf-8" \
  --data-binary @- << EOF
  {
    "query": {
      "match": {
        "summary" : {
          "query": "Peter bratislav",
          "fuzziness": "auto",
          "operator": "and"
        }
      }
    },
    "highlight": {
      "fields": {
        "summary": {}
      }
    }
  }
EOF

# nejde
curl -X GET http://localhost:9200/jobs/_search\?pretty \
    -H "Expect:" \
    -H "Content-Type: application/json; charset=utf-8" \
    --data-binary @- << EOF
    {
      "query": {
        "bool": {
          "must" : {
            "prefix": { "name": "Peter Šento" }
          },
          "should" : {
            "prefix": { "position": "stolárstvo" } }
        }
      }
    }
EOF

# test

query() {
  curl -X GET http://localhost:9200/jobs/_search\?pretty \
    -H "Expect:" \
    -H "Content-Type: application/json; charset=utf-8" \
    --data-binary @- << EOF
    {
      "query": {
        "match": {
          "all" : {
            "query": "$1",
            "fuzziness": "auto",
            "operator": "and"
          }
        }
      },
      "highlight": {
        "pre_tags": ["<strong>"], 
        "post_tags": ["</strong>"], 
        "fields": {
          "all": {}
        }
      }
    }
EOF
}

query_part() {
  curl -X GET http://localhost:9200/jobs/_search?pretty \
  -H "Expect:" \
  -H "Content-Type: application/json; charset=utf-8" \
  --data-binary @- << EOF
  {
    "suggest": {
      "name-suggest-fuzzy": {
        "prefix": "$1",
        "completion": {
          "field": "name.completion",
          "fuzzy": {
            "fuzziness": "auto"
          }
        }
      },
      "position-suggest-fuzzy": {
        "prefix": "$1",
        "completion": {
          "field": "position.completion",
          "fuzzy": {
            "fuzziness": "auto"
          }
        }
      },
      "city-suggest-fuzzy": {
        "prefix": "$1",
        "completion": {
          "field": "city.completion",
          "fuzzy": {
            "fuzziness": "auto"
          }
        }
      }
    }
  }
EOF
}

curl -X GET http://localhost:9200/jobs/_search\?pretty \
    -H "Expect:" \
    -H "Content-Type: application/json; charset=utf-8" \
    --data-binary @- << EOF
    {
      "query": {
        "bool": {
          "must" : {
            "match": {
              "summary" : {
                "query" : "Peter Šento",
                "fuzziness": "auto",
                "operator": "and"
              }
            }
          },
          "should" : {
            "prefix": { "summary": "st" } }
        }
      }
    }
EOF

curl -X GET http://localhost:9200/jobs/_search\?pretty \
    -H "Expect:" \
    -H "Content-Type: application/json; charset=utf-8" \
    --data-binary @- << EOF
{
  "query": {
    "query_string": {
      "fields": [
        "name",
        "position",
        "city"
      ],
      "query": "Peter Šento",
      "type": "cross_fields",
      "minimum_should_match": 2
    }
  }
}
EOF

curl -X GET http://localhost:9200/jobs/_search\?pretty \
    -H "Expect:" \
    -H "Content-Type: application/json; charset=utf-8" \
    --data-binary @- << EOF
{
  "query": {
    "query_string": {
      "fields": [
        "name",
        "position",
        "city"
      ],
      "query": "(Peter~ Rusnák~) and (red*)",
      "type": "cross_fields",
      "minimum_should_match": 2
    }
  }
}
EOF