#!/usr/bin/env bash

# start only ES
# docker run -d --rm \
# 	-p 127.0.0.1:9200:9200 \
# 	-p 127.0.0.1:9300:9300 \
# 	-v $(pwd)/data:/usr/share/elasticsearch/data \
# 	-e "discovery.type=single-node" \
# 	morione/elasticsearch:0.0.1

# # 	-v $(pwd)/config:/usr/share/elasticsearch/config \

# start app and es

docker-compose up -d elasticsearch
printf "Waiting for ElasticSearch to start ...\n"
sleep 80
printf "Indexing documents ...\n"
./create_index.sh
docker-compose up -d --build